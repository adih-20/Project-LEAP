# Project LEAP

Project LEAP is a project created by students at McNeil High School for the [2020-2021 NASA App Development Challenge](https://www.nasa.gov/sites/default/files/atoms/files/fy20_adc_guide.pdf "2020-2021 NASA App Development Challenge"). 

Originally a program created for a competition, this has been released publicly (under the AGPLv3 licence) in the hopes that it can be useful.

Development will be continued in the short-term.

## Features

- Mission Functionality
- (Android) VR Capability
- Data Visualizations
  - Elevation Angle Map
  - Height Map
  - Illumination Map
  - Azimuth Angle Map
  - Terrain Slope Map
- Teleportation
- Mini/Megamaps

## Getting Started
### Prerequisites
- [Unity Game Engine](https://unity.com/ "Unity Game Engine")
- [Git](https://git-scm.com/ "Git")
- Code Editor (such as [Atom](https://atom.io/ "Atom") and [Visual Studio Code](https://code.visualstudio.com/ "Visual Studio Code"))

### Installation
Use git to clone this repository.
```
git clone https://github.com/adih-20/McNeil_Project-LEAP.git
```
Open the project in Unity.

------------


Alternatively, download and run the program using the releases posted in this repository.

## Usage
See [HELP.md](HELP.md "HELP.md").
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
This project is licensed under the [AGPLv3](LICENSE.md "AGPLv3").
